<?php
require_once('../model/Image.class.php');
require_once('../model/ImageDAO.class.php');
$bd = new ImageDAO();

$img = $bd->getImage($_GET['id']);

$image = $img->image;
$titre = $img->title;
$auteur = $img->author;
$theme = $img->theme;
$dimension = $img->largeur.' x '.$img->hauteur;

include('../view/downloadImage.view.php');
?>
