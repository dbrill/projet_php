<?php
class ImageDAO
{
	private $bd;

	function __construct() {
		try {
			$this->bd = new PDO('sqlite:../model/data/photo.db');
		} catch (PDOException $e) {
			die("Erreur".$e->getMessage());
		}
	}

	function getImage(int $id):Image {
		$req= "SELECT * FROM photo WHERE id='$id'";
		$sth=($this->bd)->query($req);
		$result = $sth->fetchAll(PDO::FETCH_CLASS,'Image');
		return $result[0];
	}

	function getNbImages() {
		$req= "SELECT * FROM photo;";
		$sth=($this->bd)->query($req);
		$result = $sth->fetchAll(PDO::FETCH_CLASS,'Image');
		return sizeof($result);
	}

	function getNbImagesCat(string $cat):int {
		$req= "SELECT * FROM photo WHERE theme='$cat'";
		$sth=($this->bd)->query($req);
		$result = $sth->fetchAll(PDO::FETCH_CLASS,'Image');
		return sizeof($result);
	}

	function getImagesCat(string $cat) {
		$req= "SELECT * FROM photo WHERE theme='$cat'";
		$sth=($this->bd)->query($req);
		$result = $sth->fetchAll(PDO::FETCH_CLASS,'Image');
		return $result;
	}
}

?>
